from tkinter import *
from tkinter import messagebox


def confirm_reset():
    if messagebox.askokcancel("Reset", "Reset function body to default?"):
        text_box.delete(1.0, END)
        text_box.insert(INSERT, "begin\n\nend")

def save_fun():
    print(text_box.get(1.0, END))

window = Tk()

window.title("Java test")

frame_top_1 = Frame(window, padx = 10, pady = 10)
frame_top_2 = Frame(window, padx = 10, pady = 10)
frame_left = Frame(window, padx = 10, pady = 10)
frame_right = Frame(window, padx = 10, pady = 10)
frame_bottom = Frame(window, padx = 10, pady = 10)

label_0 = Label(frame_top_1, text = "This the first task to be solved").pack()
label_1 = Label(frame_left, text = "Instruction\nWrite a Java program.").pack()

text_box = Text(frame_right)
text_box.insert(INSERT, "begin\n\nend")
text_box.pack()

button_reset = Button(frame_top_2, text = "Reset", command = confirm_reset).pack(side = RIGHT)
button_save = Button(frame_bottom, text = "Save", command = save_fun).pack(side = RIGHT)

frame_top_1.pack(side = TOP)
frame_top_2.pack(side = TOP)
frame_bottom.pack(side = BOTTOM)
frame_left.pack(side = LEFT)
frame_right.pack(side = RIGHT)


window.mainloop()


